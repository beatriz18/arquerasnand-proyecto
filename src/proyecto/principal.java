package proyecto;

import java.util.Scanner;

public class principal {

	public static void main(String[] args) {
		
		miarqueras arqueras = new miarqueras();
		mimazo mazo = new mimazo();
		
		//DIFICULTAD//
		int dificult;
		
		Scanner teclado = new Scanner(System.in);
		System.out.println("Selecciona la dificultad del juego: ");
		System.out.println("1) F�cil: ");
		System.out.println("2) Normal: ");
		System.out.println("3) Dif�cil: ");
		
		dificult = teclado.nextInt();
		arqueras.dificultad(dificult);
		mazo.dificultad(dificult);
		
		
		//TERRITORIOS//
		
		System.out.println("******TERRRITORIOS******\n");
		
		String escudoR = "Escudo rojo   ";
		String escudoV = "Escudo verde   ";
		String escudoA = "Escudo azul    ";
		
		Ciudades ciudad1 = new Ciudades("KLIFDALHOLM", escudoR, escudoV, "");
		ciudad1.PrintCiudadesInfo();
		Ciudades ciudad2 = new Ciudades("KLIFSTEINVIK", escudoR, "", "");
		ciudad2.PrintCiudadesInfo();
		Ciudades ciudad3 = new Ciudades("BEKNESVIK", escudoA, escudoR, "");
		ciudad3.PrintCiudadesInfo();
		Ciudades ciudad4 = new Ciudades("AENESHOLM", escudoA, "", "");
		ciudad4.PrintCiudadesInfo();
		Ciudades ciudad5 = new Ciudades("AESTENSAND", escudoA, escudoV, "");
		ciudad5.PrintCiudadesInfo();
		Ciudades ciudad6 = new Ciudades("BEKDALSAND",escudoV, "", "");
		ciudad6.PrintCiudadesInfo();
		Ciudades ciudad7 = new Ciudades("BEKSTENHOLM", escudoA, escudoR, escudoV);
		ciudad7.PrintCiudadesInfo();
		
	}

}
