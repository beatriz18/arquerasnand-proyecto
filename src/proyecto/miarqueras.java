package proyecto;

public class miarqueras {
	
	/* ATRIBUTOS */
	public int flechas;
    public int estado;
    public int dificultad;
    public int huida;

    
    /* GETTERS Y SETTERS */

	public int getFlechas() {
		return flechas;
	}

	public void setFlechas(int flechas) {
		this.flechas = flechas;
	}

	public int getEstado() {
		return estado;
	}

	public void setEstado(int estado) {
		this.estado = estado;
	}

	public int getDificultad() {
		return dificultad;
	}

	public void setDificultad(int dificultad) {
		this.dificultad = dificultad;
	}

	public int getHuida() {
		return huida;
	}

	public void setHuida(int huida) {
		this.huida = huida;
	}
    
	
    /* M�TODOS */
	
    /*SELECCIONA LA DIFICULTAD */
	
	public void dificultad(int dificult) {
		if (dificult == 1) {
			setFlechas(50);
			setHuida(-1);
		}
		if (dificult == 2) {
			setFlechas(48);
			setHuida(-2);
		}
		if (dificult == 3) {
			setFlechas(45);
			setHuida(-3);
		}
	}
	
	
	
	/*miarqueras(int dificult){
		if (dificult == 1) {
			setFlechas(50);
		}
		if (dificult == 2) {
			setFlechas(48);
		}
		if (dificult == 3) {
			setFlechas(45);
		}
	}*/
	

	/* RESTA LAS FLECHAS SEG�N EL ESTADO DE LAS ARQUERAS */
	public void restarFlechas() { 
    	setFlechas(flechas - dificultad);
    }
	
	public void infoArqueras() {
		System.out.println("Estado actual: " +estado);
		System.out.println("N�mero de flechas que tenemos: " +flechas);
	}
}

