package proyecto;

import java.util.ArrayList;

public class Cartas {

	public String nombre;
	public int cantidad;
	

	Cartas(String nombre, int cantidad) {
		setNombre(nombre);
		setCantidad(cantidad);
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public int getCantidad() {
		return cantidad;
	}

	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}

	public void Carta(String nombre) {
		ArrayList<String> cartas = new ArrayList<String>();

		cartas.add("OR");
		cartas.add("AND");
		cartas.add("LIKE");
		cartas.add("XOR");
		cartas.add("COUNT");
		cartas.add("NOT");
		cartas.add("BENGALA");
	}
	
	public void imprimir() {
		System.out.println("Carta que te ha tocado: " +nombre);
	}

}
