package proyecto;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

public class Baraja {
	private ArrayList<Cartas> carta;
	
	Baraja() {
		carta = new ArrayList<Cartas>(30);
		carta.add(new Cartas("OR", 1));
		carta.add(new Cartas("OR", 2));
		carta.add(new Cartas("OR", 3));
		carta.add(new Cartas("OR", 4));
		carta.add(new Cartas("OR", 5));
		
		carta.add(new Cartas("AND", 1));
		carta.add(new Cartas("AND", 2));
		carta.add(new Cartas("AND", 3));
		carta.add(new Cartas("AND", 4));
		carta.add(new Cartas("AND", 5));
		
		carta.add(new Cartas("LIKE", 1));
		carta.add(new Cartas("LIKE", 2));
		carta.add(new Cartas("LIKE", 3));
		carta.add(new Cartas("LIKE", 4));
		carta.add(new Cartas("LIKE", 5));
		
		carta.add(new Cartas("XOR", 1));
		carta.add(new Cartas("XOR", 2));
		carta.add(new Cartas("XOR", 3));
		carta.add(new Cartas("XOR", 4));
		carta.add(new Cartas("XOR", 5));
		
		carta.add(new Cartas("COUNT", 1));
		carta.add(new Cartas("COUNT", 2));
		carta.add(new Cartas("COUNT", 3));
		carta.add(new Cartas("COUNT", 4));
		carta.add(new Cartas("COUNT", 5));
		
		carta.add(new Cartas("NOT", 1));
		carta.add(new Cartas("NOT", 2));
		carta.add(new Cartas("NOT", 3));
		carta.add(new Cartas("NOT", 4));
		carta.add(new Cartas("NOT", 5));
		
		carta.add(new Cartas("BENGALA", 1));
		carta.add(new Cartas("BENGALA", 2));
		carta.add(new Cartas("BENGALA", 3));

	}
	public void imprimir() {
		for (Cartas carta : carta)
			carta.imprimir();
	}
	public void aleatorio() {
		Collections.shuffle(carta);
	}
}
