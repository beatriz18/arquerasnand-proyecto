package proyecto;

import java.util.ArrayList;
import java.util.Collections;

public class Partida {

	public static void main(String[] args) {

		/* CIUDADES */

		String escudoR = "rojo";
		String escudoV = "verde";
		String escudoA = "azul";

		Ciudades ciudad1 = new Ciudades("KLIFDALHOLM", "", escudoR, escudoV);
		ciudad1.PrintCiudadesInfo();
		Ciudades ciudad2 = new Ciudades("KLIFSTEINVIK", "", escudoR, escudoV);
		ciudad2.PrintCiudadesInfo();
		Ciudades ciudad3 = new Ciudades("BEKNESVIK", "", escudoR, "");
		ciudad3.PrintCiudadesInfo();
		Ciudades ciudad4 = new Ciudades("AENESHOLM", escudoA, "", "");
		ciudad4.PrintCiudadesInfo();
		Ciudades ciudad5 = new Ciudades("AESTENSAND", escudoA, "", escudoV);
		ciudad5.PrintCiudadesInfo();
		Ciudades ciudad6 = new Ciudades("BEKDALSAND", "", "", escudoV);
		ciudad6.PrintCiudadesInfo();
		Ciudades ciudad7 = new Ciudades("BEKSTENHOLM", escudoA, escudoR, escudoV);
		ciudad7.PrintCiudadesInfo();

		/* CARTAS */

		/* OR */
		Cartas OR1 = new Cartas("OR", 1);
		Cartas OR2 = new Cartas("OR", 2);
		Cartas OR3 = new Cartas("OR", 3);
		Cartas OR4 = new Cartas("OR", 4);
		Cartas OR5 = new Cartas("OR", 5);

		/* AND */
		Cartas AND1 = new Cartas("AND", 1);
		Cartas AND2 = new Cartas("AND", 2);
		Cartas AND3 = new Cartas("AND", 3);
		Cartas AND4 = new Cartas("AND", 4);
		Cartas AND5 = new Cartas("AND", 5);

		/* LIKE */
		Cartas LIKE1 = new Cartas("LIKE", 1);
		Cartas LIKE2 = new Cartas("LIKE", 2);
		Cartas LIKE3 = new Cartas("LIKE", 3);
		Cartas LIKE4 = new Cartas("LIKE", 4);
		Cartas LIKE5 = new Cartas("LIKE", 5);

		/* XOR */
		Cartas XOR1 = new Cartas("XOR", 1);
		Cartas XOR2 = new Cartas("XOR", 2);
		Cartas XOR3 = new Cartas("XOR", 3);
		Cartas XOR4 = new Cartas("XOR", 4);
		Cartas XOR5 = new Cartas("XOR", 5);

		/* COUNT */
		Cartas COUNT1 = new Cartas("COUNT", 1);
		Cartas COUNT2 = new Cartas("COUNT", 2);
		Cartas COUNT3 = new Cartas("COUNT", 3);
		Cartas COUNT4 = new Cartas("COUNT", 4);
		Cartas COUNT5 = new Cartas("COUNT", 5);

		/* NOT */
		Cartas NOT1 = new Cartas("NOT", 1);
		Cartas NOT2 = new Cartas("NOT", 2);
		Cartas NOT3 = new Cartas("NOT", 3);
		Cartas NOT4 = new Cartas("NOT", 4);
		Cartas NOT5 = new Cartas("NOT", 5);

		/* BENGALA */
		Cartas BENGALA1 = new Cartas("BENGALA", 1);
		Cartas BENGALA2 = new Cartas("BENGALA", 2);
		Cartas BENGALA3 = new Cartas("BENGALA", 3);

		ArrayList<Cartas> aleatorio = new ArrayList<Cartas>();
		
		//PREGUNTAR
		aleatorio.add(OR1);
		aleatorio.add(OR2);
		aleatorio.add(OR3);
		aleatorio.add(OR4);
		aleatorio.add(OR5);
		aleatorio.add(AND1);
		
		Collections.shuffle(aleatorio);
		
		
		/* ARQUERAS */
		 miarqueras miarqueras = new miarqueras();

		/* FICHAS */
		 fichas fichas = new fichas();

		/* MAZO */
		 mimazo mimazo = new mimazo();

		Baraja baraja = new Baraja();
		baraja.aleatorio();
		System.out.println("Cartas barajadas: ");
		baraja.imprimir();
	}
}
