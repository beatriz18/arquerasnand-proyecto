package proyecto;

public class cartasAleatorias {

	public static String[] GenerarCartasAleatorias(int cantidad) {
		String[] cartasAleatorias = new String[cantidad];

		String[] nombrecartas = { "Bengala", "Count", "Like", "Not", "Or", "And", "Xor" };

		/*
		 * se har� un for para sacar una carta aleatoria de las cartas que aparecen en
		 * el String creado anteriormente y un return que retornar� las cartasAleatorias
		 */

		return cartasAleatorias;
	}


	 /* se crear� una clase donde imprimir� la carta aleatoria elegida */
	public static void imprimir(String nombrecartas) {

		System.out.println("La carta que te ha tocado es: " + nombrecartas);
	}

	/* PARA USAR LA CARTA COUNT */
	public void count() {

	}

	/* PARA USAR LA CARTA LIKE */
	public void like() {

	}

	/* PARA USAR LA CARTA NOT */
	public void not() {

	}

	/* PARA USAR LA CARTA OR */
	public void or() {

	}

	/* PARA USAR LA CARTA BENGALA */
	public void bengala() {

	}

	/* PARA USAR LA CARTA XOR */
	public void xor() {

	}

	/* PARA USAR LA CARTA AND */
	public void and() {

	}

	/* PARA DESCARTAR LA CARTA UTILIZADA */
	public void descartarcarta() {

	}
}
