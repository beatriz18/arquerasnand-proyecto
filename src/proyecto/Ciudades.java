package proyecto;

class Ciudades {

	/* ATRIBUTOS */
	public String nombre;
	public String escudoA = "Escudo azul";
	public String escudoR = "Escudo rojo";
	public String escudoV = "Escudo verde";
	public int num_guerreros, num_orcos;
	public static String estado;
	
	
	/* GETTER Y SETTERS */
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getEscudoA() {
		return escudoA;
	}

	public void setEscudoA(String escudoA) {
		this.escudoA = escudoA;
	}

	public String getEscudoR() {
		return escudoR;
	}

	public void setEscudoR(String escudoR) {
		this.escudoR = escudoR;
	}

	public String getEscudoV() {
		return escudoV;
	}

	public void setEscudoV(String escudoV) {
		this.escudoV = escudoV;
	}

	public int getNum_guerreros() {
		return num_guerreros;
	}

	public void setNum_guerreros(int num_guerreros) {
		this.num_guerreros = num_guerreros;
	}

	public int getNum_orcos() {
		return num_orcos;
	}

	public void setNum_orcos(int num_orcos) {
		this.num_orcos = num_orcos;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	
	/*CONSTRUCTOR*/
	Ciudades (String nombre_ciudad, String escudoAzul, String escudoRojo, String escudoVerde) {

		setNombre(nombre_ciudad);
		setEscudoA(escudoAzul);
		setEscudoR(escudoRojo);
		setEscudoV(escudoVerde);
		setEstado("Defendida");
		
	}

	/* M�TODOS */ 
	public void ponerorcos() {
		/* aqu� pondremos los orcos que iremos colocando en las ciudad */
	}
	
	public void ponerguerreros() {
		/* aqu� pondremos los guerreros que iremos colocando en las ciudad */
	}
	
	public String Luchar() {
		/* aqu� pondremos el estado actual de la ciudad */
		return estado;
	}
	
	
	public void PrintCiudadesInfo() {
		System.out.println(nombre);
		System.out.printf(escudoA, escudoV, escudoR);
		System.out.println(escudoV);
		System.out.println(escudoR);
		System.out.println("\n-Estado de la ciudad: " +estado);
		System.out.println("-N�mero de orcos que tiene la ciudad: " +num_orcos);
		System.out.println("-N�mero de guerreros que tiene la ciudad: " +num_guerreros);
		System.out.println("\n");
		
	}
	
	public static void main(String[] args) {

		String escudoR = "Escudo rojo";
		String escudoV = "Escudo verde";
		String escudoA = "Escudo azul";
		
		Ciudades ciudad1 = new Ciudades("KLIFDALHOLM", "", escudoR, escudoV);
		ciudad1.PrintCiudadesInfo();
		Ciudades ciudad2 = new Ciudades("KLIFSTEINVIK", "", escudoR, escudoV);
		ciudad2.PrintCiudadesInfo();
		Ciudades ciudad3 = new Ciudades("BEKNESVIK", "", escudoR, "");
		ciudad3.PrintCiudadesInfo();
		Ciudades ciudad4 = new Ciudades("AENESHOLM", escudoA, "", "");
		ciudad4.PrintCiudadesInfo();
		Ciudades ciudad5 = new Ciudades("AESTENSAND", escudoA, "", escudoV);
		ciudad5.PrintCiudadesInfo();
		Ciudades ciudad6 = new Ciudades("BEKDALSAND", "", "", escudoV);
		ciudad6.PrintCiudadesInfo();
		Ciudades ciudad7 = new Ciudades("BEKSTENHOLM", escudoA, escudoR, escudoV);
		ciudad7.PrintCiudadesInfo();
	}

}

